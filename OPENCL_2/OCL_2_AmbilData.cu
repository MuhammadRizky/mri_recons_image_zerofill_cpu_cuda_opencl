//reading an entire binary file
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector>
#include <cmath>
#include <sys/time.h>
//CUDA Include(s)
// includes, project

//OPENCL Include
#include <clFFT.h>
// #include "common.h"

//OPENCV Include(s)
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#define RANK 1
#define M_PI 3.14159265358979323846

struct kernelConf
{
    dim3 block;
    dim3 grid;
};

using namespace std;
using namespace cv;

//Functions prototype
//CPU Operation
const char *getErrorString(cl_int error);
void readHeaderFile(char *filen, int * dims);
int getCFLSize(char *filen, streampos &size);
int readCflFile(char *file, float2* data);
int getSpecificSliceData(float2* input, float2* out, int spec_slice, int spec_bit, const int *dim, int x_new, int y_new);
void dataScaling(float* data, int size);
int DoDisplayImage2CV(float* dataRaw, float* dataRecons, int xdim, int ydim, int spec_slice, int zipdimx, char* filename, int numSlice);

// GPU Operation
bool checkSuccess(cl_int errorNumber);
cl_int oclGetPlatformID(cl_platform_id* clSelectedPlatformID);
cl_device_id create_device();
bool createProgram(cl_context context, cl_device_id device, string filename, cl_program* program);
int OCLZeroFilled(cl_mem d_inputOneSlice, cl_mem d_inputOneSliceNew, int xdim, int ydim, int bitdim, int numSlice, int xdim_new, int ydim_new, cl_command_queue commandQueue, cl_program program, int numThread);
int OCL_clfft(cl_mem inputData, int xdim, int ydim, int bitdim, cl_context context, cl_command_queue commandQueue);
int OCL_fftshift(cl_mem inputData, int xdim, int ydim, int bitdim, int numSLice, cl_command_queue commandQueue, cl_program program);
int DoRECONSOperation( cl_mem inputData, float* d_out, int *dim, int numSlice, cl_context context, cl_command_queue commandQueue, cl_program program, float* times, int numThread);
int OCL_clfft(cl_mem inputData, int xdim, int ydim, int bitdim, int numSLice, cl_context context, cl_command_queue commandQueue);
int OCL_RSS(cl_mem inputData, cl_mem outData, int sizeOneImage, int xdim, int ydim, int bitdim, int numSLice, cl_command_queue commandQueue, cl_program program, int numThread);
int DoRSSOCLOperation(cl_mem inputData, float* output, const int* dim, int size, int spec_slice, int numSlice, cl_context  context, cl_command_queue commandQueue, cl_program program, int numThread, float* times);

int main(int argc, char* argv[])
{
if(argc < 6)
	{
		cout << "<Usage> <path/filename> <Spec_slice> <Number Slice(s)> <New X dimension (Zero Filled Dimension)> <Number Thread (32 atau 512)>" << endl;
		return 1;
	}
	cout << "========================================================================================================================" << endl;
	//Initializing Timer
	clock_t GPU_start1, GPU_end1;
	//=============================================================================================Inisialisasi OpenCL============================================================
	//OpenCL Structures
	cl_device_id device;
	cl_context context;
	cl_command_queue commandQueue;
	cl_program program;
	const unsigned int numberMemoryObjects = 2;
	cl_mem memObj_data[numberMemoryObjects] = {0,0};
	int err;

	// printf("Create Device\n");
	device = create_device();
	// printf("Create Context\n");
	context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);

	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << endl;
		exit(1);
	}

	// printf("Create Command Queue\n");
	commandQueue = clCreateCommandQueue(context, device, 0, &err);
	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << endl;
		exit(1);
	}

	if(!createProgram(context, device, "device_code/Recons.cl", &program))
	{
		cerr << "Failed to create a program OpenCL" << __FILE__ << ":" << __LINE__ << endl;
		return 1;
	}
	char *filename = (char*)malloc(100);
	int *dim = (int*)malloc(sizeof(int)*4);
	int *Newdim = (int*)malloc(sizeof(int)*4);
	float *timesRecons = (float*)malloc(sizeof(float)*4);
	float *timesRaw = (float*)malloc(sizeof(float)*2);
	sprintf(filename,"%s",argv[1]);
	int slice = atoi(argv[2]);
	int numSlice = atoi(argv[3]);
	int new_x = atoi(argv[4]);
	int numThread = atoi(argv[5]);

	if (new_x < 512)
	{
		cout << "ZIP Dimension Less than 512" << endl;
		new_x = 512;
	}
	else
	{
		int x_comp = 1;
		while(x_comp < new_x)
		{
			x_comp <<= 1;
		}

		if(x_comp > new_x)
		{
			cout << "Zero Filled Dimension Must be Power of TWO" << endl;
			return 1;
		}
	}
	// cout << "filename " << filename << endl;
	//=============================================================================================READ HEADER FILE============================================================
	//Input		: Filename
	//Output	: array contains 4 pointer int
	//Return 	: Void / None
	readHeaderFile(filename, dim);
	//=============================================================================================READ HEADER FILE============================================================
	int sizeCfl = 1;
	for(int i = 0; i < 4; i++)
	{
		sizeCfl *= dim[i];
		Newdim[i] = dim[i];
	}
	// cout << "Size CFL " << std::to_string(sizeCfl) << endl;
	streampos size;
	int ret2 = getCFLSize(filename, size);
	if(ret2 == 1)
	{
		cout << "Error on reading CFL File" << endl;
		return 1;
	}
	size_t buffer_size = sizeCfl * sizeof(float2);
	float2 *data = (float2*)malloc(buffer_size);
	//=============================================================================================READ CFL FILE============================================================
	//Input		: filename
	//Output	: float2 Data pointer
	//Return 	: Int - 0 > ReadCflFile SUCCESS, 1 < ReadCflFile FAILED
	int ret = readCflFile(filename, data);
	if(ret == 1)
	{
		cout << "Error on Reading CFL File" << endl;
		return 1;
	}
	//=============================================================================================READ CFL FILE============================================================
	/*for(int i = 0; i < 4; i++)
	{
		cout << "Dimension " << std::to_string(i+1) << " : " << std::to_string(dim[i]) << endl;
	}*/

	
	
	int xdim = dim[0];
	int ydim = dim[1];
	int bitdim = dim[3];
	int xdim_new = new_x;
	int ydim_new = xdim_new;
	int sizeImageNew = xdim_new*ydim_new;
	int sizeImage = xdim*ydim;
	int sizeImageNewSlice = sizeImageNew* bitdim * numSlice;
	int sizeImageNewSliceRaw = sizeImage* bitdim * numSlice;

	size_t nBytes_CNew = sizeof(float2)*sizeImageNewSlice;
	size_t nBytes_C = sizeof(float2)*sizeImageNewSliceRaw;
	size_t nBytes_F = sizeof(float)*sizeImageNew*numSlice;
	size_t nBytes_FRSS = sizeof(float)*sizeImage*numSlice;
	memObj_data[0] = clCreateBuffer(context, CL_MEM_READ_WRITE, nBytes_C, NULL, &err);
	checkSuccess(err);
	
	float2* dataManySlice = (float2*)malloc(nBytes_C);

	GPU_start1  = clock();
	// GPU_start1  = clock();
	//=============================================================================================GET SPECIFIC SLICE DATA============================================================
	//input 	:	float2 Data Pointer,
	//				float2 Data One Slice Pointer for Zero Filling Interpolation,
	//				Specific Slice,
	//				Raw Data Dimension
	//Output	:	float2 Data One Slice Pointer
	//Return 	:	Void / None
	err = getSpecificSliceData(data, dataManySlice, slice, numSlice, dim, xdim_new, ydim_new);
	if(err == 1)
	{
		cerr << "Failed to Get Specific Slice(s) Data" << __FILE__ << ":" << __LINE__ << endl;
		exit(1);
	}
	//=============================================================================================GET SPECIFIC SLICE DATA============================================================
	free(data);
	err = clEnqueueWriteBuffer(commandQueue, memObj_data[0], CL_TRUE, 0, nBytes_C, dataManySlice, 0, NULL, NULL);
	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}

	free(dataManySlice);

	GPU_end1 = clock();
	float GPUTimer_getspec = (float)(GPU_end1 - GPU_start1)/CLOCKS_PER_SEC;
	GPU_start1  = clock();

	memObj_data[1] = clCreateBuffer(context, CL_MEM_READ_WRITE, nBytes_CNew, NULL, &err);
	checkSuccess(err);

	cl_uint initValue = 0;
	err = clEnqueueFillBuffer(commandQueue, memObj_data[1], &initValue, sizeof(cl_uint), 0, nBytes_CNew, 0, NULL, NULL);
	clFinish(commandQueue);

	if(!checkSuccess(err))
	{
		cerr << "Failed to Fill Buffer " << __FILE__ << ":" << __LINE__ << endl;
		cerr << getErrorString(err) << endl;
		exit(1);
	}
	//=============================================================================================ZERO FILLED INTERPOLATION============================================================
	//input 	:	float2 Data Raw Pointer,
	//				xdim, ydim, bitdim - Raw Data Dimension,
	//				xdim_new, ydim_new - ZIP Dimension,
	//				Number Slice(s),
	//				Command Queue OCL,
	//				Program OCL
	//Output	:	float2 Data Many Slice Pointer
	//Return 	:	Void / None
	err = OCLZeroFilled(memObj_data[0], memObj_data[1], xdim, ydim, bitdim, numSlice, xdim_new, ydim_new, commandQueue, program, numThread);
	clFinish(commandQueue);
	if(err == 1)
	{
		cerr << "Failed to do Zero Filled - Interpolation" << __FILE__ << ":" << __LINE__ << endl;
		exit(1);
	}
	//=============================================================================================ZERO FILLED INTERPOLATION============================================================
	Newdim[0] = xdim_new;
	Newdim[1] = ydim_new;

	/*cout << "NEW dimension for Zero Filling Interpolation" << endl;
	for(int i = 0; i < 4; i++)
	{
		cout << "Dimension " << std::to_string(i+1) << " : " << std::to_string(Newdim[i]) << endl;
	}*/
	xdim = Newdim[0];
	ydim = Newdim[1];
	bitdim = Newdim[3];
	
	GPU_end1 = clock();
	float GPUTimer_zeroFill = (float)(GPU_end1 - GPU_start1)/CLOCKS_PER_SEC;
	// GPU_start1  = clock();
	
	float* dataCUFFT_F = (float*)malloc(nBytes_F);
	//=============================================================================================DO RECONSTRUCTION CUDA OPERATION============================================================
	//input 	:	DATA After ZIP Operation,
	//				DATA Output FLOAT,
	//				New Dimension Array,
	//				Number Slice,
	//				Float Array for Timing for each process,
	//				CommandQueue OCL,
	//				Program OCL	
	//Output	: 	Float Data After Reconstruction Operation		
	//Return 	:	Int - 0 > DoRECONSOperation SUCCESS, 1 < DoRECONSOperation FAILED	
	int retOCLFFT = DoRECONSOperation(memObj_data[1], dataCUFFT_F, Newdim, numSlice, context, commandQueue, program, timesRecons, numThread);
	if(retOCLFFT == 1)
	{
		cout << "OCL DoRECONSOperation Operation is FAILED" << endl;
		return 1;
	}
	//=============================================================================================DO RECONSTRUCTION CUDA OPERATION============================================================
	clReleaseMemObject(memObj_data[1]);
	
	// GPU_end1 = clock();
	// float GPUTimer_fft = (float)(GPU_end1 - GPU_start1)/CLOCKS_PER_SEC;
	float GPUTimer_fft = timesRecons[0]+timesRecons[1]+timesRecons[2]+timesRecons[3];
	// GPU_start1  = clock();

	int xdim_old = dim[0];
	int ydim_old = dim[1];
	float* rss = (float*)malloc(nBytes_FRSS);
	//=============================================================================================DO RSS FOR RAW OPERATION============================================================
	//Input		: 	cplx Data Pointer,
	//				Float Data Pointer (For Result RSS Operation)
	//				Raw Data Dimension from Header File,
	//				Specific Slice,
	//				Number Slice(s),
	//				CommandQueue OCL,
	//				Program OCL
	//Output	:	Float Data Pointer (For Result RSS Operation)				
	//Return 	:	Int - 0 = DoRSSOCLOperation SUCCESS, 1 = DoRSSOCLOperation FAILED
	int retRSS = DoRSSOCLOperation(memObj_data[0], rss, dim, sizeCfl, slice, numSlice, context, commandQueue, program, numThread, timesRaw);
	if(retRSS == 1)
	{
		cout << "RSS Operation is FAILED" << endl;
		return 1;
	}
	//=============================================================================================DO RSS FOR RAW OPERATION============================================================
	clReleaseMemObject(memObj_data[0]);

	// GPU_end1 = clock();
	float GPUTimer_RSSRaw = timesRaw[0];
	float GPUTimer_ScaleRaw = timesRaw[1];
	float GPUTimer = GPUTimer_getspec + GPUTimer_zeroFill+ GPUTimer_fft + GPUTimer_RSSRaw + GPUTimer_ScaleRaw;
	cout << "==========================================================================================================" << endl;
	cout << "<path/filename> <Start Slice> <Number Slice Per Operatio> <New X dimension (Zero Filled Dimension)> <Number Thread (32 or 512>" << endl;
	cout << "<kspace> " << std::to_string(slice) << " " << std::to_string(numSlice) << " " << std::to_string(new_x) << " " << std::to_string(numThread)<< endl;
	cout << "Timing <GetSpec> <ZIP> <FFT> <SHIFT> <RSS> <SCALING> <RSS RAW> <SCALING_RAW> <TOTAL>" << endl;
	cout << "Timing " << std::to_string(GPUTimer_getspec*1000) << " " << std::to_string(GPUTimer_zeroFill*1000) << " " << std::to_string(timesRecons[0]*1000) << " " << std::to_string(timesRecons[1]*1000)<<" " << std::to_string(timesRecons[2]*1000) << " " << std::to_string(timesRecons[3]*1000) << " " <<std::to_string(GPUTimer_RSSRaw*1000) <<" " <<std::to_string(GPUTimer_ScaleRaw*1000) << " " << std::to_string(GPUTimer*1000)<< endl;
	/*cout << "Timing - Get Specific Slice Data on CPU Processor : " << std::to_string(GPUTimer_getspec*1000) << " ms" << endl;
	cout << "Timing - Zero Filled - Interpolation on GPU Processor : " << std::to_string(GPUTimer_zeroFill*1000) << " ms" << endl;
	cout << "Timing - FFT + FFTSHIFT on CPU Processor : " << std::to_string(GPUTimer_fft*1000) << " ms" << endl;
	cout << "         Timing - FFT on GPU Processor : " << std::to_string(timesRecons[0]*1000) << " ms" << endl;
	cout << "         Timing - FFTSHIFT on GPU Processor : " << std::to_string(timesRecons[1]*1000) << " ms" << endl;
	cout << "         Timing - RSS on GPU Processor : " << std::to_string(timesRecons[2]*1000) << " ms" << endl;
	cout << "         Timing - Data Scaling on CPU Processor : " << std::to_string(timesRecons[3]*1000) << " ms" << endl;
	cout << "Timing - RSS Operation on GPU Processor : " << std::to_string(GPUTimer_RSSRaw*1000) << " ms" << endl;
	cout << "OPENCL II GPU Timing using CPU Timer : " << std::to_string(GPUTimer*1000) << " ms" << endl;*/
	// cout << "==========================================================================================================" << endl;
	//=============================================================================================DO Display Image to CV============================================================
	//input 	:	DATA RAW,
	//				DATA RECONS,
	//				xdim , ydim - Initial Dimension,
	//				slice - Specific Slice,
	//				xdim_new - ZIP Dimension,
	//				Number Slice,
	//Output	: 	-		
	//Return 	:	Int - 0 > DoDisplayImage2CV SUCCESS, 1 < DoDisplayImage2CV FAILED	
	int retCV = DoDisplayImage2CV(rss, dataCUFFT_F, xdim_old, ydim_old, slice, xdim_new, filename, numSlice);
	if(retCV == 1)
	{
		cout << "Display Image is Failed" << endl;
		return 1;
	}
	//=============================================================================================DO Display Image to CV============================================================
	
	clReleaseProgram(program);
	clReleaseCommandQueue(commandQueue);
	clReleaseContext(context);
	clReleaseDevice(device);
	//FREE MEMORY
	free(filename);
	free(timesRecons);
	free(timesRaw);
	free(Newdim);
	free(dim);
	free(rss);
	free(dataCUFFT_F);
	return 0;
}

void readHeaderFile(char *filen, int * dims)
{
	// cout << "Read Header File" << endl;
	char path[20];
	sprintf(path,"%s.hdr",filen);
	FILE *myFile;
	myFile = fopen(path,"r");
	string line;
	streampos size;
	
	fseek(myFile, 13, SEEK_SET);
	for(int i = 0; i < 4; i++)
	{
		fscanf(myFile,"%d",&dims[i]);
	}
}

int getCFLSize(char *filen, streampos &size)
{
	// cout << "Get Size CFL File" << endl;
	char path[20];
	sprintf(path,"%s.cfl",filen);
	ifstream file(path, ios::in | ios::binary | ios::ate);
	if(file.is_open())
	{
		size = file.tellg();
	}
	else
	{
		cout << "Unable to open file";
		return 1; 
	}
	return 0;
}

int readCflFile(char *filen, float2* data)
{
	// cout << "Read CFL File" << endl;
	streampos size;
	char path[20];
	sprintf(path,"%s.cfl",filen);
	ifstream file(path, ios::in | ios::binary | ios::ate);
	if(file.is_open())
	{
		size = file.tellg();
		if(size)
		{
			// cout << "Read CFL file Success" << endl;
		}
		else
		{
			cout << "Error Allocating Memory" << endl;
			return 1;
		}
		// cout << "Contains Size : "<< std::to_string(size) << endl;
	}
	else
	{
		cout << "Unable to open file";
		return 1; 
	}

	if(file.is_open())
	{
		file.seekg(0, ios::beg);
		file.read((char*)data, size);
		file.close();
	}
	return 0;
}



int getSpecificSliceData(float2* input, float2* out, int spec_slice, int numSlice, const int* dim, int xdim_new, int ydim_new )
{
	// cout << "Get Specific Slice Data" << endl;
	int xdim = dim[0];
	int ydim = dim[1];
	int slicedim = dim[2];
	int bitdim = dim[3];
	int sizeOneImage = xdim*ydim;
	int offsetSlices = sizeOneImage*slicedim;
	int index = 0;

	for(int m = 0; m < bitdim; m++)
	{
		for(int l = spec_slice; l < (spec_slice+numSlice); l++)
		{
			for(int k = 0; k < ydim; k++)
			{
				for(int j = 0; j < xdim; j++)
				{
					out[index]  = input[j + (k*xdim) + (l*sizeOneImage) + (m*offsetSlices)];
					index++;
				}
			}
		}
	}
	return 0;
}

int OCLZeroFilled(cl_mem d_inputOneSlice, cl_mem d_inputOneSliceNew, int xdim, int ydim, int bitdim, int numSlice, int xdim_new, int ydim_new, cl_command_queue commandQueue, cl_program program, int numThread)
{
	// printf("Do OpenCL Zero Filled - Interpolation\n");
	int err = 0;
	//Create Kernel for Zero Filled
	cl_kernel kernel;
	kernel = clCreateKernel(program, "DoZeroFilledOCL", &err);
	checkSuccess(err);
	//Setup The Kernel Arguments
	bool isKernelArgSuccess = true;
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_inputOneSlice));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_inputOneSliceNew));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 2, sizeof(int), &xdim_new));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 3, sizeof(int), &xdim));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 4, sizeof(int), &ydim));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 5, sizeof(int), &bitdim));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 6, sizeof(int), &numSlice));

	if(!isKernelArgSuccess)
	{
		cerr << "Failed to Set Kernel Argument(s)" << __FILE__ << ":" << __LINE__ << endl;
		exit(1);
	}
	int maxWI = numThread;
	int workItem_x = xdim;
	int workItem_y = ydim;
	int workGroup_x = 1;
	int workGroup_y = 1;
	if(xdim > maxWI)
	{
		workItem_x = maxWI;
		workItem_y = maxWI;
		workGroup_x = ceil(double(xdim)/double(workItem_x));
		workGroup_y = ceil(double(ydim)/double(workItem_y));
	}

	// size_t workItemSize[2] = {workItem_x, workItem_y};
	size_t workGlobalSize[2] = {workGroup_x*workItem_x, workGroup_y*workItem_y};

	//Enqueue The DoZeroFilled Kernel
	err = clEnqueueNDRangeKernel(commandQueue, kernel, 2, NULL, workGlobalSize, 0, 0, NULL, NULL);
	//Wait for kernel execution completion
	if(!checkSuccess(clFinish(commandQueue)))
	{
		cerr << "Failed waiting for kernel execution completion" << __FILE__ << ":" << __LINE__ << endl;
		return 1;
	}
	if(!checkSuccess(err))
	{
		cerr << "Failed enqueuing the kernel. " << __FILE__ << ":"<< __LINE__ << endl;
        return 1;
	}

	if(!checkSuccess(clReleaseKernel(kernel)))
	{
		cerr << "Failed to Release Kernel OCL ZERO FILLED" << __FILE__ << ":" << __LINE__ << endl;
		return 1;
	}
	
	return 0;
}

int DoDisplayImage2CV(float* dataRaw, float* dataRecons, int xdim, int ydim, int spec_slice, int zipdimx, char* filename, int numSlice)
{
	// cout << "DO Display Image" << endl;
	char winNameRaw[100];
	char winNameRecons[100];
	cv::Mat imgRaw;
	cv::Mat imgRecons;
	int oneDimRaw = xdim*ydim;
	int oneDim = zipdimx*zipdimx;
	size_t nBytes_One = sizeof(float)*oneDim;
	size_t nBytes_OneRaw = sizeof(float)*oneDimRaw;
	float* oneImage = (float*)malloc(nBytes_One);
	float* oneImageRaw = (float*)malloc(nBytes_OneRaw);
	int offset = 0;
	int offsetRaw = 0;

	for(int a = 0; a < numSlice; a++)
	{
		offset = a*oneDim;
		offsetRaw = a*oneDimRaw;
		memcpy(oneImage, dataRecons + offset, nBytes_One);
		memcpy(oneImageRaw, dataRaw + offsetRaw, nBytes_OneRaw);
		imgRaw = cv::Mat(xdim, ydim, CV_32F, oneImageRaw);
		imgRecons = cv::Mat(zipdimx, zipdimx, CV_32F, oneImage);
		if(imgRaw.rows == 0 || imgRaw.cols == 0)
			return 1;
		if(imgRecons.rows == 0 || imgRecons.cols == 0)
			return 1;
		sprintf(winNameRecons,"Reconstructed Image on CV - Slice %d",spec_slice+a );
		sprintf(winNameRaw,"Raw Image on CV - Slice %d",spec_slice+a );
		cv::namedWindow(winNameRaw, CV_WINDOW_KEEPRATIO | CV_WINDOW_NORMAL);
		cv::namedWindow(winNameRecons, CV_WINDOW_KEEPRATIO | CV_WINDOW_NORMAL);
		cv::imshow(winNameRaw,imgRaw);
		cv::waitKey(500);
		cv::imshow(winNameRecons,imgRecons);
		cv::waitKey(1500);
	}
	cv::waitKey();
	free(oneImage);
	free(oneImageRaw);
	
	return 0;
}

int DoRECONSOperation( cl_mem inputData, float* output, int *dim, int numSlice, cl_context context, cl_command_queue commandQueue, cl_program program, float* times, int numThread)
{
	// cout << "DO CUFFT RSS Operation" << endl;
	int xdim = dim[0];
	int ydim = dim[1];
	int err = 0;
	clock_t start, end;
	// cout << "XDIM " << std::to_string(xdim) << " YDIM " << std::to_string(ydim) << endl;
	int bitdim = dim[3];
	int sizeOneImage = xdim*ydim;
	int sizeManySlices = sizeOneImage*numSlice;
	size_t nBytes_F = sizeof(float)*sizeManySlices;

	cl_mem d_out = clCreateBuffer(context, CL_MEM_READ_WRITE, nBytes_F, NULL, &err);
	checkSuccess(err);

	start = clock();
	err = OCL_clfft(inputData, xdim, ydim, bitdim, numSlice, context, commandQueue);
	if(err != 0)
	{
		cerr << "Failed to do clFFT" << __FILE__ << ":" << __LINE__ << endl;
		return 1;
	}
	end = clock();
	times[0] = (float)(end-start)/CLOCKS_PER_SEC;
	start = clock();

	err = OCL_fftshift(inputData, xdim, ydim, bitdim, numSlice, commandQueue, program);
	if(err != 0)
	{
		cerr << "Failed to do OCL FFT Shift" << __LINE__ << endl;
		return 1;
	}

	end = clock();
	times[1] = (float)(end-start)/CLOCKS_PER_SEC;
	start = clock();

	err = OCL_RSS(inputData, d_out, sizeOneImage, xdim, ydim, bitdim, numSlice, commandQueue, program, numThread);
	if(err != 0)
	{
		cerr << "Failed to do RSS Operation" << __LINE__ << endl;
		return 1;
	}

	end = clock();
	times[2] = (float)(end-start)/CLOCKS_PER_SEC;
	start = clock();

	err = clEnqueueReadBuffer(commandQueue, d_out, CL_TRUE, 0, nBytes_F, output, 0, NULL, NULL);
	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}
	dataScaling(output, sizeManySlices);

	clReleaseMemObject(d_out);

	end = clock();
	times[3] = (float)(end-start)/CLOCKS_PER_SEC;

	return 0;
}

int OCL_clfft(cl_mem inputData, int xdim, int ydim, int bitdim, int numSlice, cl_context context, cl_command_queue commandQueue)
{
	// printf("DO OCL CLFFT\n");
	int err;
	int N = xdim * ydim;
	size_t offset = 0;
	// size_t batch = 1;
	size_t nBytes = sizeof(float2)*N;
	cl_mem temp = clCreateBuffer(context, CL_MEM_READ_WRITE, nBytes, NULL, &err);
	checkSuccess(err);
	//clFFT library start declarations
	clfftPlanHandle planHandle;
	clfftDim dim = CLFFT_1D;
	size_t clLengths[RANK] = {N};

	//Setup clFFT
	clfftSetupData fftSetup;
	err = clfftInitSetupData(&fftSetup);
	err = clfftSetup(&fftSetup);
	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;

		return 1;
	}
	err = clfftCreateDefaultPlan(&planHandle, context, dim, clLengths);
	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}
	err = clfftSetPlanPrecision(planHandle, CLFFT_SINGLE);
	// err = clfftSetPlanScale( planHandle, CLFFT_BACKWARD, 1 );
	err = clfftSetLayout(planHandle, CLFFT_COMPLEX_INTERLEAVED, CLFFT_COMPLEX_INTERLEAVED);
	err = clfftSetResultLocation(planHandle, CLFFT_INPLACE);
	// err = clfftSetPlanBatchSize(planHandle, batch);
	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}
	err = clfftBakePlan(planHandle, 1, &commandQueue, NULL, NULL);

	for(int i = 0; i < (bitdim*numSlice); i++)
	{
		offset = nBytes*i;
		err = clEnqueueCopyBuffer(commandQueue, inputData, temp, offset, 0, nBytes, 0, NULL, NULL);
		err = clfftEnqueueTransform(planHandle, CLFFT_BACKWARD, 1, &commandQueue, 0, NULL, NULL, &temp, NULL, NULL);
		err = clFinish(commandQueue);
		err = clEnqueueCopyBuffer(commandQueue, temp, inputData, 0, offset, nBytes, 0, NULL, NULL);
	}
	err = clFinish(commandQueue);
	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}
	err = clfftDestroyPlan(&planHandle);
	checkSuccess(err);
	clfftTeardown();
	clReleaseMemObject(temp);

	return 0;
}

int OCL_fftshift(cl_mem inputData, int xdim, int ydim, int bitdim, int numSlice, cl_command_queue commandQueue, cl_program program)
{
	// printf("DO OCL FFT SHIFT\n");
	int err;
	cl_kernel kernel;
	kernel = clCreateKernel(program, "DoOCLFFTShift", &err);
	if(err != 0)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}

	bool isKernelArgSuccess = true;

	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 0, sizeof(cl_mem), &inputData));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 1, sizeof(int), &xdim));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 2, sizeof(int), &bitdim));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 3, sizeof(int), &numSlice));

	if(!isKernelArgSuccess)
	{
		cerr << "Failed to set Kernel Argument(s)" << __LINE__ << endl;
		return 1;
	}

	int maxWI = 32;
	int workItem_x = xdim;
	int workItem_y = ydim;
	int workGroup_x = 1;
	int workGroup_y = 1;
	if(xdim > maxWI)
	{
		workItem_x = maxWI;
		workItem_y = maxWI;
		workGroup_x = ceil(double(xdim)/double(workItem_x));
		workGroup_y = ceil(double(ydim)/double(workItem_y));
	}

	size_t workItemSize[2] = {workItem_x, workItem_y};
	size_t workGlobalSize[2] = {workGroup_x*workItem_x, workGroup_y*workItem_y};

	//Enqueue The DoOCLFFTSHIFT Kernel
	err = clEnqueueNDRangeKernel(commandQueue, kernel, 2, NULL, workGlobalSize, workItemSize, 0, NULL, NULL);

	if(!checkSuccess(clFinish(commandQueue)))
	{
		cerr << "Failed waiting for kernel execution completion" << __LINE__ << endl;
		return 1;
	}
	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}

	if(!checkSuccess(clReleaseKernel(kernel)))
	{
		cerr << "Failed to Release Kernel OCL FFT SHIFT" << __FILE__ << ":" << __LINE__ << endl;
		return 1;
	}
	return 0;

}

int OCL_RSS(cl_mem inputData, cl_mem outData, int sizeOneImage, int xdim, int ydim, int bitdim, int numSlice, cl_command_queue commandQueue, cl_program program, int numThread)
{
	// printf("Do OCL RSS OPERATION\n");
	int err;

	cl_kernel kernel;
	kernel = clCreateKernel(program, "DoOCLRSS", &err);

	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}

	bool isKernelArgSuccess = true;

	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 0, sizeof(cl_mem), &inputData));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 1, sizeof(cl_mem), &outData));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 2, sizeof(int), &sizeOneImage));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 3, sizeof(int), &xdim));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 4, sizeof(int), &ydim));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 5, sizeof(int), &bitdim));
	isKernelArgSuccess &= checkSuccess(clSetKernelArg(kernel, 6, sizeof(int), &numSlice));

	if(!isKernelArgSuccess)
	{
		cerr << "Failed to set Kernel Argument(s)" << __LINE__ << endl;
		return 1;
	}

	int maxWI = numThread;
	int workItem_x = sizeOneImage;
	int workGroup_x = 1;
	if(sizeOneImage > maxWI)
	{
		workItem_x = maxWI;
		workGroup_x = ceil(double(sizeOneImage)/double(workItem_x));
	}

	// size_t workItemSize[1] = {workItem_x};
	size_t workGlobalSize[1] = {workGroup_x*workItem_x};

	err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, 0, workGlobalSize, 0, 0, NULL, NULL);

	if(!checkSuccess(clFinish(commandQueue)))
	{
		cerr << "Failed waiting for Kernel Execution Completion" << __LINE__ << endl;
		return 1;
	}

	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}

	if(!checkSuccess(clReleaseKernel(kernel)))
	{
		cerr << "Failed to Release Kernel OCL RSS RECONS" << __FILE__ << ":" << __LINE__ << endl;
		return 1;
	}

	return 0;
}

void dataScaling(float* data, int size)
{
	// cout << "Do Data Scaling" << endl;
	float max = 0;
	for(int i = 0; i< size; i++)
	{
		if(data[i] > max)
		{
			max = data[i];
		}
	}

	for(int j = 0; j < size; j++)
	{
		data[j] = data[j]/max;
	}
}

int DoRSSOCLOperation(cl_mem inputData, float* output, const int* dim, int size, int spec_slice, int numSlice, cl_context  context, cl_command_queue commandQueue, cl_program program, int numThread, float* times)
{
	// cout << "Do RSS (Only for Raw Data) Operation" << endl;
	//Initilising
	clock_t start, end;

	start = clock();

	int xdim = dim[0];
	int ydim = dim[1];
	int err = 0;
	// cout << "XDIM " << std::to_string(xdim) << " YDIM " << std::to_string(ydim) << endl;
	int bitdim = dim[3];
	int sizeOneImage = xdim*ydim;
	size_t nBytes_F = sizeof(float)*sizeOneImage*numSlice;

	cl_mem d_out = clCreateBuffer(context, CL_MEM_READ_WRITE, nBytes_F, NULL, &err);
	checkSuccess(err);

	err = OCL_RSS(inputData, d_out, sizeOneImage, xdim, ydim, bitdim, numSlice, commandQueue, program, numThread);
	if(err != 0)
	{
		cerr << "Failed to do RSS Operation" << __LINE__ << endl;
		return 1;
	}

	end = clock();
	times[0] = (float)(end - start) / (CLOCKS_PER_SEC);

	start = clock();


	err = clEnqueueReadBuffer(commandQueue, d_out, CL_TRUE, 0, nBytes_F, output, 0, NULL, NULL);
	if(err != CL_SUCCESS)
	{
		cerr << getErrorString(err) << __LINE__ << endl;
		return 1;
	}	

	int sizeManySlices = sizeOneImage*numSlice;
	dataScaling(output, sizeManySlices);

	clReleaseMemObject(d_out);

	end = clock();
	times[1] = (float)(end - start) / (CLOCKS_PER_SEC);
	
	return 0;
}

//Find a GPU or CPU asscociated with the first available platform
cl_device_id create_device()
{
	cl_platform_id platform;
	cl_device_id device;
	int err;
	char platform_name[128];
    char device_name[128];
	//Identify a Platform
	//
	// err = clGetPlatformIDs(1, &platform, NULL);
	err = oclGetPlatformID(&platform);
	size_t ret_param_size = 0;
    err = clGetPlatformInfo(platform, CL_PLATFORM_NAME,
            sizeof(platform_name), platform_name,
            &ret_param_size);
	if(err < 0)
	{
		perror("Couldn't identify a Platform");
		exit(1);
	}
	else
	{
		printf("CL_PLATFORM_NAME : \t%s\n", platform_name );
	}


	//Access a device
	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL );
	err = clGetDeviceInfo(device, CL_DEVICE_NAME,
            sizeof(device_name), device_name,
            &ret_param_size);
    printf("Device found on the above platform: %s\n", device_name);
	if(err < 0)
	{
		cerr << getErrorString(err) << __FILE__ << ":" << __LINE__ << endl;
		exit(1);
	}

	return device;
}

bool createProgram(cl_context context, cl_device_id device, string filename, cl_program* program)
{
    cl_int errorNumber = 0;
    ifstream kernelFile(filename.c_str(), ios::in);

    if(!kernelFile.is_open())
    {
        cerr << "Unable to open " << filename << ". " << __FILE__ << ":"<< __LINE__ << endl;
        return false;
    }

    /*
     * Read the kernel file into an output stream.
     * Convert this into a char array for passing to OpenCL.
     */
    ostringstream outputStringStream;
    outputStringStream << kernelFile.rdbuf();
    string srcStdStr = outputStringStream.str();
    const char* charSource = srcStdStr.c_str();

    *program = clCreateProgramWithSource(context, 1, &charSource, NULL, &errorNumber);
    if (!checkSuccess(errorNumber) || program == NULL)
    {
        cerr << "Failed to create OpenCL program. " << __FILE__ << ":"<< __LINE__ << endl;
        return false;
    }

    /* Try to build the OpenCL program. */
    bool buildSuccess = checkSuccess(clBuildProgram(*program, 0, NULL, NULL, NULL, NULL));

    /* Get the size of the build log. */
    size_t logSize = 0;
    clGetProgramBuildInfo(*program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);

    /*
     * If the build succeeds with no log, an empty string is returned (logSize = 1),
     * we only want to print the message if it has some content (logSize > 1).
     */
    if (logSize > 1)
    {
        char* log = new char[logSize];
        clGetProgramBuildInfo(*program, device, CL_PROGRAM_BUILD_LOG, logSize, log, NULL);

        string* stringChars = new string(log, logSize);
        // cerr << "Build log:\n " << *stringChars << endl;

        delete[] log;
        delete stringChars;
    }

    if (!buildSuccess)
    {
        clReleaseProgram(*program);
        cerr << "Failed to build OpenCL program. " << __FILE__ << ":"<< __LINE__ << endl;
        return false;
    }

    return true;
}

bool checkSuccess(cl_int errorNumber)
{
    if (errorNumber != CL_SUCCESS)
    {
        cerr << "OpenCL error: " << getErrorString(errorNumber) << endl;
        return false;
    }
    return true;
}

const char *getErrorString(cl_int error)
{
switch(error){
    // run-time and JIT compiler errors
    case 0: return "CL_SUCCESS";
    case -1: return "CL_DEVICE_NOT_FOUND";
    case -2: return "CL_DEVICE_NOT_AVAILABLE";
    case -3: return "CL_COMPILER_NOT_AVAILABLE";
    case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case -5: return "CL_OUT_OF_RESOURCES";
    case -6: return "CL_OUT_OF_HOST_MEMORY";
    case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case -8: return "CL_MEM_COPY_OVERLAP";
    case -9: return "CL_IMAGE_FORMAT_MISMATCH";
    case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case -11: return "CL_BUILD_PROGRAM_FAILURE";
    case -12: return "CL_MAP_FAILURE";
    case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
    case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case -15: return "CL_COMPILE_PROGRAM_FAILURE";
    case -16: return "CL_LINKER_NOT_AVAILABLE";
    case -17: return "CL_LINK_PROGRAM_FAILURE";
    case -18: return "CL_DEVICE_PARTITION_FAILED";
    case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

    // compile-time errors
    case -30: return "CL_INVALID_VALUE";
    case -31: return "CL_INVALID_DEVICE_TYPE";
    case -32: return "CL_INVALID_PLATFORM";
    case -33: return "CL_INVALID_DEVICE";
    case -34: return "CL_INVALID_CONTEXT";
    case -35: return "CL_INVALID_QUEUE_PROPERTIES";
    case -36: return "CL_INVALID_COMMAND_QUEUE";
    case -37: return "CL_INVALID_HOST_PTR";
    case -38: return "CL_INVALID_MEM_OBJECT";
    case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case -40: return "CL_INVALID_IMAGE_SIZE";
    case -41: return "CL_INVALID_SAMPLER";
    case -42: return "CL_INVALID_BINARY";
    case -43: return "CL_INVALID_BUILD_OPTIONS";
    case -44: return "CL_INVALID_PROGRAM";
    case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
    case -46: return "CL_INVALID_KERNEL_NAME";
    case -47: return "CL_INVALID_KERNEL_DEFINITION";
    case -48: return "CL_INVALID_KERNEL";
    case -49: return "CL_INVALID_ARG_INDEX";
    case -50: return "CL_INVALID_ARG_VALUE";
    case -51: return "CL_INVALID_ARG_SIZE";
    case -52: return "CL_INVALID_KERNEL_ARGS";
    case -53: return "CL_INVALID_WORK_DIMENSION";
    case -54: return "CL_INVALID_WORK_GROUP_SIZE";
    case -55: return "CL_INVALID_WORK_ITEM_SIZE";
    case -56: return "CL_INVALID_GLOBAL_OFFSET";
    case -57: return "CL_INVALID_EVENT_WAIT_LIST";
    case -58: return "CL_INVALID_EVENT";
    case -59: return "CL_INVALID_OPERATION";
    case -60: return "CL_INVALID_GL_OBJECT";
    case -61: return "CL_INVALID_BUFFER_SIZE";
    case -62: return "CL_INVALID_MIP_LEVEL";
    case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
    case -64: return "CL_INVALID_PROPERTY";
    case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
    case -66: return "CL_INVALID_COMPILER_OPTIONS";
    case -67: return "CL_INVALID_LINKER_OPTIONS";
    case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

    // extension errors
    case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
    case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
    case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
    case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
    case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
    case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
    default: return "Unknown OpenCL error";
    }
}

cl_int oclGetPlatformID(cl_platform_id* clSelectedPlatformID)
{
    char chBuffer[1024];
    cl_uint num_platforms; 
    cl_platform_id* clPlatformIDs;
    cl_int ciErrNum;
    *clSelectedPlatformID = NULL;

    // Get OpenCL platform count
    ciErrNum = clGetPlatformIDs (0, NULL, &num_platforms);
    if (ciErrNum != CL_SUCCESS)
    {
        printf(" Error %i in clGetPlatformIDs Call !!!\n\n", ciErrNum);
        return -1000;
    }
    else 
    {
        if(num_platforms == 0)
        {
            printf("No OpenCL platform found!\n\n");
            return -2000;
        }
        else 
        {
            // if there's a platform or more, make space for ID's
            if ((clPlatformIDs = (cl_platform_id*)malloc(num_platforms * sizeof(cl_platform_id))) == NULL)
            {
                printf("Failed to allocate memory for cl_platform ID's!\n\n");
                return -3000;
            }

            // get platform info for each platform and trap the NVIDIA platform if found
            ciErrNum = clGetPlatformIDs (num_platforms, clPlatformIDs, NULL);
            for(cl_uint i = 0; i < num_platforms; ++i)
            {
                ciErrNum = clGetPlatformInfo (clPlatformIDs[i], CL_PLATFORM_NAME, 1024, &chBuffer, NULL);
                if(ciErrNum == CL_SUCCESS)
                {
                    if(strstr(chBuffer, "NVIDIA") != NULL)
                    {
                        *clSelectedPlatformID = clPlatformIDs[i];
                        break;
                    }
                }
            }

            // default to zeroeth platform if NVIDIA not found
            if(*clSelectedPlatformID == NULL)
            {
                printf("WARNING: NVIDIA OpenCL platform not found - defaulting to first platform!\n\n");
                *clSelectedPlatformID = clPlatformIDs[0];
            }

            free(clPlatformIDs);
        }
    }

    return CL_SUCCESS;
}