@ECHO OFF
rem set /p filename= Enter The Specific File Name :
rem set /p exec= Enter The Specific Executable File Name :
rem ECHO Pengambilan Data %filename% file
for %%m IN (512 1024 2048) do (
	echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
	echo %%m
	for %%s in (4) do (
		echo "............................................................................................................."
		echo slice %%s
		for %%t in (32) do (
			echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			echo thread = %%t
			for /l %%i in (1,1,5) do (
				echo Iterasi ke-%%i
				echo nvprof --metrics gld_requested_throughput --metrics gst_requested_throughput CUDA_1_AmbilData_2.exe kspace 127 %%s %%m %%t
				nvprof --metrics gld_requested_throughput --metrics gst_requested_throughput CUDA_1_AmbilData_2.exe kspace 127 %%s %%m %%t
				)
			)
		)
	)
