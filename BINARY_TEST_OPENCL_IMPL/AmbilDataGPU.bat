@ECHO OFF
rem set /p filename= Enter The Specific File Name :
rem set /p exec= Enter The Specific Executable File Name :
rem ECHO Pengambilan Data %filename% file
for %%m IN (512 1024 2048) do (
	echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
	echo %%m
	for /l %%s in (2,2,4) do (
		echo "............................................................................................................."
		echo slice %%s
		for %%t in (32 512) do (
			echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			echo thread = %%t
			for /l %%i in (1,1,20) do (
				echo Iterasi ke-%%i
				echo OCL_2_AmbilData_2.exe kspace 127 %%s %%m %%t
				OCL_2_AmbilData_2.exe kspace 127 %%s %%m %%t
				)
			)
		)
	)
