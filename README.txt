Folder:
CPU 		>> Code CPU Implementation and Makefile
CUDA_1 		>> Code CUDA I Implementation and Makefile
CUDA_2 		>> Code CUDA II Implementation and Makefile
OPENCL_1 	>> Code OPENCL I Implementation and Makefile
OPENCL_2 	>> Code OPENCL II Implementation and Makefile

BINARY_TEST_CUDA_IMPL_AND_CPU_IMPL		>> Test Binary File of CPU Implementation,
						   CUDA I Implementation,
						   CUDA II Implementation.

BINARY_TEST_OPENCL_IMPL				>> Test Binary File of OPENCL I Implementation,
						   OPENCL II Implementation.

NVIDIA_VISUAL_PROFILER_FILE			>> Profiling Files on NVIDIA VISUAL PROFILER
						   for CUDA I Implementation and
						   CUDA II Implementation.
						   Open those files on NVIDIA VISUAL PROFILER.


HOW TO TEST THESE CODE IMPLEMENTATION :

1.Open Command Prompt/Terminal on directory of CPU, CUDA_1, CUDA_2, OPENCL_1, or OPENCL_2. 
2. Type "make" and fileName of CU File (without dot and name of extension file). i.e. make CUDA_1_AmbilData
3. Copy File Executable to folder "BINARY_TEST_CUDA_IMPL_AND_CPU_IMPL" for 
   executable file of CUDA Implementation and CPU Implementation, or
   folder "BINARY_TEST_OPENCL_IMPL" for executable file of OPENCL Implementation.
4. Copy RAW DATA KSPACE (Download at http://mridata.org/knees/fully_sampled/p4/e1/s1/P4.zip)
   where executable file is located, on "BINARY_TEST_CUDA_IMPL_AND_CPU_IMPL" folder, or
   folder "BINARY_TEST_OPENCL_IMPL".
5. Open Command Prompt / Terminal where executable file is located, then
   For Executable file of CPU Implementation : type "executable_file.exe <KSPACE path/filename> <Start Slice> <Number Slice Per Operatio>" (without quotaion mark),
   i.e. type on Command Prompt/terminal :
		CPU_AmbilData.exe kspace 127 4 512
   For Executable file of CUDA Implementation or OPENCL Implementation : type "executable_file.exe <KSPACE path/filename> <Start Slice> <Number Slice Per Operatio> <New X dimension (Zero Filled Dimension)> <Number Thread (32 or 512>" (without quotaion mark),
   i.e. type on Command Prompt/terminal :
		CUDA_1_AmbilData.exe kspace 127 4 512 32
6. If NVIDIA GPU on your PC/NOTEBOOK, you will get the images output. 
7. DONE.

Tested on Windows 8.1 64-bit.