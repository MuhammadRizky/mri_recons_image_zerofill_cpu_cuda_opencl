//reading an entire binary file
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector>
#include <cmath>
#include <sys/time.h>
//CUDA Include(s)
// includes, project
#include <cufft.h>

//OPENCV Include(s)
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


//FFTW Include(s)
#include <fftw3.h>

typedef float2 cplx;

#define RANK 1
#define M_PI 3.14159265358979323846
#define fftshift(out, in, x, y, bitdim, numSlice) circshift(out, in, x, y, bitdim, (x/2), (y/2), numSlice)

//Functions prototype
//CPU Operation
void readHeaderFile(char *filen, int * dims);
int readCflFile(char *file, cplx* data);
void getSpecificSliceData(cplx* input, cplx* rawOut, int spec_slice, int spec_bit, const int *dim, int x_new, int y_new);
void CPUZeroFilled(cplx* rawOut, cplx* out, int spec_slice, int numSlice, const int* dim, int xdim_new, int ydim_new );
void dataScaling(float* data, int size);
int DoDisplayImage2CV(float* dataRaw, float* dataRecons, int xdim, int ydim, int spec_slice, int zipxdim, int numSlice);
void circshift(cplx *out, cplx *in, int xdim, int ydim, int bitdim, int xshift, int yshift, int numSlice);
void DoRSSCPU(cplx* input, float* out, int N, int x, int y, int bitdim, int numSlice);
int DoRECONSOperation( cplx* DatafftOneSlice, float* out, int *dim, int numSlice, float* times);

using namespace std;
using namespace cv;

int main(int argc, char* argv[])
{
	if(argc < 5)
	{
		cout << "<Usage> <path/filename> <Start Slice> <Number Slice Per Operatio> <New X dimension (Zero Filled Dimension)>" << endl;
		return 1;
	}

	//Initializing Timer
	clock_t CPU_start1, CPU_end1;
	char *filename = (char*)malloc(100);
	int *dim = (int*)malloc(sizeof(int)*4);
	int *Newdim = (int*)malloc(sizeof(int)*4);
	float *timesRecons = (float*)malloc(sizeof(float)*4);
	float *timesRaw = (float*)malloc(sizeof(float)*2);
	sprintf(filename,"%s",argv[1]);

	int slice = atoi(argv[2]);
	int numSlice = atoi(argv[3]);
	int new_x = atoi(argv[4]);

	if (new_x < 512)
	{
		cout << "ZIP Dimension Less than 512" << endl;
		new_x = 512;
	}
	else
	{
		int x_comp = 1;
		while(x_comp < new_x)
		{
			x_comp <<= 1;
		}
		if(x_comp > new_x)
		{
			cout << "Zero Filled Dimension Must be Power of TWO" << endl;
			return 1;
		}
	}

	// cout << "filename " << filename << endl;
	//=============================================================================================READ HEADER FILE============================================================
	//Input		: Filename
	//Output	: array contains 4 pointer int
	//Return 	: Void / None
	readHeaderFile(filename, dim);
	//=============================================================================================READ HEADER FILE============================================================

	int sizeCfl = 1;
	for(int i = 0; i < 4; i++)
	{
		sizeCfl *= dim[i];
		Newdim[i] = dim[i];
	}
	// cout << "Size CFL " << std::to_string(sizeCfl) << endl;
	cplx *data = (cplx*)malloc(sizeof(cplx)*sizeCfl);

	//=============================================================================================READ CFL FILE============================================================
	//Input		: filename
	//Output	: cplx Data pointer
	//Return 	: Int - 0 > ReadCflFile SUCCESS, 1 < ReadCflFile FAILED
	int ret = readCflFile(filename, data);
	if(ret == 1)
	{
		cout << "Error on Reading CFL File" << endl;
		return 1;
	}
	//=============================================================================================READ CFL FILE============================================================
	/*for(int i = 0; i < 4; i++)
	{
		cout << "Dimension " << std::to_string(i+1) << " : " << std::to_string(dim[i]) << endl;
	}*/


	
	
	
	int xdim = dim[0];
	int ydim = dim[1];
	int bitdim = dim[3];
	int xdim_new = new_x;
	int ydim_new = xdim_new;
	int sizeImageNew = xdim_new*ydim_new * numSlice;
	int sizeImage = xdim*ydim*numSlice;
	int sizeImageNewSlice = sizeImageNew* bitdim;
	int sizeImageNewSliceRaw = xdim*ydim* bitdim * numSlice;

	size_t nBytes_C = sizeof(cplx)*sizeImageNewSlice;
	size_t nBytes_CRaw = sizeof(cplx)*sizeImageNewSliceRaw;
	size_t nBytes_F = sizeof(float)*sizeImageNew;
	size_t nBytes_FRSS = sizeof(float)*sizeImage;
	cplx* dataManySlice = (cplx*)malloc(nBytes_C);
	cplx* dataManySliceRaw = (cplx*)malloc(nBytes_CRaw);
	memset(dataManySlice, 0, nBytes_C);

	CPU_start1  = clock();
	//=============================================================================================GET SPECIFIC SLICE DATA & ZERO INTERPOLATION============================================================
	//input 	:	cplx Data Pointer,
	//				cplx Data Many Slice Pointer for Zero Filling Interpolation,
	//				Specific Slice,
	//				Number Slice(s)
	//				Raw Data Dimension
	//				ZIP Dimension - X
	//				ZIP Dimension - Y
	//Output	:	cplx Data Many Slice Pointer
	//Return 	:	Void / None
	getSpecificSliceData(data, dataManySliceRaw, slice, numSlice, dim, xdim_new, ydim_new);
	//=============================================================================================GET SPECIFIC SLICE DATA============================================================
	//
	//
	free(data);
	Newdim[0] = xdim_new;
	Newdim[1] = ydim_new;

	/*cout << "NEW dimension for Zero Filling Interpolation" << endl;
	for(int i = 0; i < 4; i++)
	{
		cout << "Dimension " << std::to_string(i+1) << " : " << std::to_string(Newdim[i]) << endl;
	}*/
	xdim = dim[0];
	ydim = dim[1];
	bitdim = dim[3];
	int sizeManyImage = xdim*ydim*numSlice;
	
	CPU_end1 = clock();
	float CPUTimer_getspec = (float)(CPU_end1 - CPU_start1)/CLOCKS_PER_SEC;
	// float CPUTimer_getspec = timesRaw[0] + timesRaw[1];
	// 
	CPU_start1  = clock();

	CPUZeroFilled(dataManySliceRaw, dataManySlice,  slice, numSlice, dim, xdim_new, ydim_new);

	CPU_end1 = clock();
	float CPUTimer_zip = (float)(CPU_end1 - CPU_start1)/CLOCKS_PER_SEC;

	// CPU_start1  = clock();
	
	
	float* dataFFT_F = (float*)malloc(nBytes_F);
	//=============================================================================================DO RECONSTRUCTION OPERATION============================================================
	//input 	:	DATA After ZIP Operation,
	//				DATA Output FLOAT,
	//				New Dimension Array,
	//				Number Slice,
	//				Float Array for Timing for each process,	
	//Output	: 	Float Data After Reconstruction Operation		
	//Return 	:	Int - 0 > DoCUFFTOperation SUCCESS, 1 < DoCUFFTOperation FAILED	
	int retCUFFT = DoRECONSOperation(dataManySlice, dataFFT_F, Newdim, numSlice, timesRecons);
	if(retCUFFT == 1)
	{
		cout << "CPU RECONSTRUCTION Operation is FAILED" << endl;
		return 1;
	}
	//=============================================================================================DO RECONSTRUCTION OPERATION============================================================
	//
	// CPU_end1 = clock();
	float CPUTimer_fft = timesRecons[0]+timesRecons[1]+timesRecons[2]+timesRecons[3];

	CPU_start1  = clock();

	float *rss = (float*)malloc(nBytes_FRSS);
	//=============================================================================================DO RAW IMAGE OPERATION============================================================
	//Input		: 	cplx RAW Data Pointer,
	//				Float Data Pointer (For Result RSS Operation)
	//				Size For all Slice,
	//				Raw Data Dimension from Header File,
	//				Specific Slice,
	//Output	:	Float Data Pointer (For Result RSS Operation)				
	//Return 	:	void
	DoRSSCPU(dataManySliceRaw, rss, sizeManyImage, xdim, ydim, bitdim, numSlice);
	//=============================================================================================DO RAW IMAGE OPERATION============================================================
	//
	CPU_end1 = clock();
	float CPUTimer_RSSRaw = (float)(CPU_end1 - CPU_start1)/CLOCKS_PER_SEC;

	CPU_start1 = clock();

	dataScaling(rss, sizeManyImage);

	CPU_end1 = clock();
	float CPUTimer_ScaleRaw = (float)(CPU_end1- CPU_start1)/(CLOCKS_PER_SEC);

	// float CPUTimer_Raw = timesRaw[0] + timesRaw[1];
	float CPUTimer = CPUTimer_getspec + CPUTimer_zip + CPUTimer_fft + CPUTimer_RSSRaw + CPUTimer_ScaleRaw;

	cout << "==========================================================================================================" << endl;
	cout << "<path/filename> <Start Slice> <Number Slice Per Operatio> <New X dimension (Zero Filled Dimension)>" << endl;
	cout << "<kspace> " << std::to_string(slice) << " " << std::to_string(numSlice) << " " << std::to_string(new_x) << endl;
	cout << "Timing <GetSpec> <ZIP> <FFT> <SHIFT> <RSS> <SCALING_RECONS> <RSS RAW> <SCALING_RAW> <TOTAL>" << endl;
	cout << "Timing " << std::to_string(CPUTimer_getspec*1000) << " " << std::to_string(CPUTimer_zip*1000) << " " << std::to_string(timesRecons[0]*1000) << " " << std::to_string(timesRecons[1]*1000)<<" " << std::to_string(timesRecons[2]*1000) << " " << std::to_string(timesRecons[3]*1000) << " " <<std::to_string(CPUTimer_RSSRaw *1000) << " " <<std::to_string(CPUTimer_ScaleRaw*1000) << " " << std::to_string(CPUTimer*1000)<< endl;
	//
	//
	//=============================================================================================DO Display Image to CV============================================================
	//input 	:	DATA RAW,
	//				DATA RECONS,
	//				xdim , ydim - Initial Dimension,
	//				slice - Specific Slice,
	//				xdim_new - ZIP Dimension,
	//				Number Slice,
	//Output	: 	-		
	//Return 	:	Int - 0 > DoDisplayImage2CV SUCCESS, 1 < DoDisplayImage2CV FAILED	
	int retCV = DoDisplayImage2CV(rss, dataFFT_F, xdim, ydim, slice, xdim_new, numSlice);
	if(retCV == 1)
	{
		cout << "Display Image is Failed" << endl;
		return 1;
	}
	//=============================================================================================DO Display Image to CV============================================================
	//
	//FREE MEMORY
	free(filename);
	free(dim);
	free(Newdim);
	free(timesRecons);
	free(timesRaw);
	free(rss);
	free(dataManySlice);
	free(dataManySliceRaw);
	free(dataFFT_F);

	return 0;
}

void readHeaderFile(char *filen, int * dims)
{
	// cout << "Read Header File" << endl;
	char path[20];
	sprintf(path,"%s.hdr",filen);
	FILE *myFile;
	myFile = fopen(path,"r");
	string line;
	streampos size;
	
	fseek(myFile, 13, SEEK_SET);
	for(int i = 0; i < 4; i++)
	{
		fscanf(myFile,"%d",&dims[i]);
	}

}

int readCflFile(char *filen, cplx* data)
{
	// cout << "Read CFL File" << endl;
	streampos size;
	char path[20];
	sprintf(path,"%s.cfl",filen);
	ifstream file(path, ios::in | ios::binary | ios::ate);
	if(file.is_open())
	{
		size = file.tellg();
		// cout << "Contains Size : "<< std::to_string(size) << endl;
	}
	else
	{
		cout << "Unable to open file";
		return 1; 
	}

	if(file.is_open())
	{
		file.seekg(0, ios::beg);
		file.read((char*)data, size);
		file.close();
	}
	return 0;
}

void dataScaling(float* data, int size)
{
	// cout << "Do Data Scaling" << endl;
	float max = 0;
	for(int i = 0; i< size; i++)
	{
		if(data[i] > max)
		{
			max = data[i];
		}
	}

	for(int j = 0; j < size; j++)
	{
		data[j] = data[j]/max;
	}
}

void getSpecificSliceData(cplx* input, cplx* rawOut, int spec_slice, int numSlice, const int* dim, int xdim_new, int ydim_new )
{
	// cout << "Get Specific Slice Data" << endl;
	// clock_t start,end;
	int xdim = dim[0];
	int ydim = dim[1];
	int slicedim = dim[2];
	int bitdim = dim[3];
	int sizeOneImage = xdim*ydim;
	// int residue = (xdim_new-xdim)/2;
	// int sidx = (xdim_new*residue)+residue;
	// int offset = 2*residue;
	// int offsetPerBitmask = numSlice*xdim_new*ydim_new;
	// int offsetPerSlice = xdim_new*ydim_new;
	int index = 0;

	// start = clock();

	for(int m = 0; m < bitdim; m++)
	{
		for(int l = spec_slice; l < (spec_slice+numSlice); l++)
		{
			for(int k = 0; k < ydim; k++)
			{
				for(int j = 0; j < xdim; j++)
				{
					rawOut[index]  = input[j + (k*xdim) + (l*sizeOneImage) + (m*slicedim*sizeOneImage)];
					index++;
				}
			}
		}
	}

	// end = clock();
	// times[0] = (float)(end-start)/CLOCKS_PER_SEC;
	// start = clock();

	// for(int m = 0; m < bitdim; m++)
	// {
	// 	for(int l = 0; l < numSlice; l++)
	// 	{
	// 		for(int i = 0; i < ydim ; i++)
	// 		{
	// 			for(int j = 0; j < xdim; j++)
	// 			{
	// 				out[sidx + j + (i*xdim) + (i*offset) + (l*offsetPerSlice) +(m*offsetPerBitmask)] = rawOut[j + (i*xdim) + (l*sizeOneImage) + (m*numSlice*sizeOneImage)];
	// 			}
	// 		}
	// 	}
	// }

	// end = clock();
	// times[1] = (float)(end-start)/CLOCKS_PER_SEC;
}

void CPUZeroFilled(cplx* rawOut, cplx* out, int spec_slice, int numSlice, const int* dim, int xdim_new, int ydim_new )
{
	// cout << "Get Specific Slice Data" << endl;
	// clock_t start,end;
	int xdim = dim[0];
	int ydim = dim[1];
	int bitdim = dim[3];
	int sizeOneImage = xdim*ydim;
	int residue = (xdim_new-xdim)/2;
	int sidx = (xdim_new*residue)+residue;
	int offset = 2*residue;
	int offsetPerBitmask = numSlice*xdim_new*ydim_new;
	int offsetPerSlice = xdim_new*ydim_new;

	// start = clock();
	// end = clock();
	// times[0] = (float)(end-start)/CLOCKS_PER_SEC;
	// start = clock();

	for(int m = 0; m < bitdim; m++)
	{
		for(int l = 0; l < numSlice; l++)
		{
			for(int i = 0; i < ydim ; i++)
			{
				for(int j = 0; j < xdim; j++)
				{
					out[sidx + j + (i*xdim) + (i*offset) + (l*offsetPerSlice) +(m*offsetPerBitmask)] = rawOut[j + (i*xdim) + (l*sizeOneImage) + (m*numSlice*sizeOneImage)];
				}
			}
		}
	}

	// end = clock();
	// times[1] = (float)(end-start)/CLOCKS_PER_SEC;
}

void DoRSSCPU(cplx* input, float* out, int N, int x, int y, int bitdim, int numSlice)
{
	
	float *temp = (float*)malloc(sizeof(float)*N);
	for(int m = 0; m < bitdim; m++)
	{
		for(int i = 0; i < numSlice ; i++)
		{
			for(int j = 0; j < y; j++)
			{
				for(int k = 0; k < x; k++)
				{
					out[k + (j*x) + (i*y*x)] = cuCabsf(input[k + (j*x)+ (i*y*x) + (m*x*y*numSlice)]);
					out[k + (j*x) + (i*y*x)] = (float)pow((out[k + (j*x) + (i*y*x)]),2);
					temp[k + (j*x) +(i*y*x)] += out[k + (j*x) + (i*y*x)];
				}
			}
		}
	}
	for(int i = 0; i < numSlice ; i++)
	{
		for(int j = 0; j < y; j++)
		{
			for(int k = 0; k < x; k++)
			{
				out[k + (j*x)+ (i*y*x)] = 0;
				out[k + (j*x)+ (i*y*x)] = sqrt(temp[k + (j*x)+ (i*y*x)]);
			}
		}
	}
	free(temp);

}

int DoDisplayImage2CV(float* dataRaw, float* dataRecons, int xdim, int ydim, int spec_slice, int zipdimx, int numSlice)
{
	// cout << "DO Display Image" << endl;
	char winNameRaw[100];
	char winNameRecons[100];
	cv::Mat imgRaw;
	cv::Mat imgRecons;

	int oneDimRaw = xdim*ydim;
	int oneDim = zipdimx*zipdimx;
	size_t nBytes_One = sizeof(float)*oneDim;
	size_t nBytes_OneRaw = sizeof(float)*oneDimRaw;

	// cout << "Before Malloc" << endl;
	float* oneImage = (float*)malloc(nBytes_One);
	float* oneImageRaw = (float*)malloc(nBytes_OneRaw);
	int offset = 0;
	int offsetRaw = 0;
	// cout << "After Malloc" << endl;
	for(int a = 0; a < numSlice; a++)
	{
		offset = a*oneDim;
		offsetRaw = a*oneDimRaw;
		memcpy(oneImage, dataRecons + offset, nBytes_One);
		memcpy(oneImageRaw, dataRaw + offsetRaw, nBytes_OneRaw);
		imgRaw = cv::Mat(xdim, ydim, CV_32F, oneImageRaw);
		imgRecons = cv::Mat(zipdimx, zipdimx, CV_32F, oneImage);
		if(imgRaw.rows == 0 || imgRaw.cols == 0)
			return 1;
		if(imgRecons.rows == 0 || imgRecons.cols == 0)
			return 1;
		sprintf(winNameRecons,"Reconstructed Image on CV - Slice %d",spec_slice+a );
		sprintf(winNameRaw,"Raw Image on CV - Slice %d",spec_slice+a );
		cv::namedWindow(winNameRaw, CV_WINDOW_KEEPRATIO | CV_WINDOW_NORMAL);
		cv::namedWindow(winNameRecons, CV_WINDOW_KEEPRATIO | CV_WINDOW_NORMAL);
		cv::imshow(winNameRaw,imgRaw);
		cv::waitKey(500);
		cv::imshow(winNameRecons,imgRecons);
		cv::waitKey(2000);
	}
	cv::waitKey();
	free(oneImage);
	free(oneImageRaw);
	return 0;
}

int DoRECONSOperation( cplx* DatafftManySlice, float* out, int *dim, int numSlice, float* times)
{
	// cout << "DO CPU FFT RSS Operation" << endl;
	int xdim = dim[0];
	int ydim = dim[1];
	// cout << "XDIM " << std::to_string(xdim) << " YDIM " << std::to_string(ydim) << endl;
	// int slicedim = dim[2];
	int bitdim = dim[3];
	int sizeOneSlice = xdim*ydim*bitdim;
	int sizeManySlice = sizeOneSlice*numSlice;
	int sizeManyImage = xdim*ydim*numSlice;
	int sizeOneImage = xdim*ydim;

	size_t nBytes_C = sizeof(cplx)*sizeManySlice;
	cplx* temp_in = (cplx*)malloc(nBytes_C);
	
	clock_t start, end;

	start = clock();
	//FFTW PLAN
	fftwf_plan pfftw;

	//FFTW plan dft 1D
	//Advanced Complex DFT
	int n[] = {sizeOneImage};
	int howmany = bitdim*numSlice;
	int idist = sizeOneImage;
	int odist = sizeOneImage;
	int istride = 1;
	int ostride = 1;
	int *onembed = NULL;
	int *inembed = NULL;

	pfftw = fftwf_plan_many_dft(RANK, n, howmany, 
								reinterpret_cast<fftwf_complex*>(DatafftManySlice),
								inembed, istride, idist, 
								reinterpret_cast<fftwf_complex*>(DatafftManySlice),
								onembed, ostride, odist,
								FFTW_BACKWARD,
								FFTW_ESTIMATE);
	fftwf_execute(pfftw);

	fftwf_destroy_plan(pfftw);

	fftwf_cleanup();

	end = clock();
    times[0] = (float)(end-start)/CLOCKS_PER_SEC;
    start = clock(); 

	memcpy(temp_in, DatafftManySlice, nBytes_C);
	fftshift(DatafftManySlice, temp_in, xdim, ydim, bitdim, numSlice);

	free(temp_in);

	end = clock();
    times[1] = (float)(end-start)/CLOCKS_PER_SEC;
    start = clock();
	DoRSSCPU(DatafftManySlice, out, sizeManyImage, xdim, ydim, bitdim, numSlice);

	end = clock();
    times[2] = (float)(end-start)/CLOCKS_PER_SEC;
    start = clock();

	dataScaling(out, sizeManyImage);

	end = clock();
    times[3] = (float)(end-start)/CLOCKS_PER_SEC;
	

	return 0;
}

void circshift(cplx *out, cplx *in, int xdim, int ydim, int bitdim, int xshift, int yshift, int numSlice)
{
	int N = xdim*ydim;
	int Bitm = N*numSlice;
	for(int m = 0; m < bitdim; m++)
	{
		for(int l = 0; l < numSlice ; l++)
		{
		  for (int i =0; i < xdim; i++) {
		    int ii = (i + xshift) % xdim;
		    for (int j = 0; j < ydim; j++) {
		      int jj = (j + yshift) % ydim;
		      out[ii * ydim + jj + (l*N) + (m*Bitm)] = in[i * ydim + j + (l*N) + (m*Bitm)];
		    }
		  }
		}
	}
}